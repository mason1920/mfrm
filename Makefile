PREFIX=/usr/local

CGI_BIN=/usr/lib/cgi-bin
SHARE=$(PREFIX)/share

install:
	mkdir -p $(CGI_BIN)
	mkdir -p $(SHARE)/mfrm
	pkexec chown www-data $(SHARE)/mfrm
	pkexec chgrp www-data $(SHARE)/mfrm
	cp -r cgi-bin/* $(CGI_BIN)/
	chmod +x $(CGI_BIN)/*
